#!/bin/bash

if [ -z $1 ] ; then
    echo "Please enter hostname"
    exit
fi

hostname=$1

curl -XDELETE "http://$hostname:9200/deals-companies"
echo
curl -X PUT "http://$hostname:9200/deals-companies" -d '
{ 
   "settings":{  
      "index":{  
         "number_of_shards":"5",
         "number_of_replicas":"1"
      }
   }, 
   "mappings":{  
      "doc":{  
         "properties":{  
            "nameSuggest":{  
               "type":"completion",
               "analyzer":"simple",
               "search_analyzer":"simple",
               "payloads":true
            }
         }
      }
   }
}'
