'''
Created on 17-Mar-2017

@author: factweavers
'''
from elasticsearch import Elasticsearch
import json




indexName = "deals-companies"
indexType = "doc"
bulkData = []
client = Elasticsearch(hosts=["localhost:9200"], timeout = 30)

def bulkIndex():
    res = client.bulk(bulkData)

'''with open("/home/factweavers/Downloads/yelpFiles/yelp_academic_dataset_review.json") as f:
    for line in f:
        print type(json.loads(line))'''

generator = (line for line in open('/home/factweavers/Downloads/companies.json', 'r'))
count = 1
for line in generator:
    
    meta = {'index' : {'_index': indexName, '_type': indexType}}
    bulkData.append(meta)
    doc = json.loads(line)
    #print "doc: ",doc
    doc["nameSuggest"] = {}
    doc["nameSuggest"]["input"] = doc["company_name"].split(" ")
    doc["nameSuggest"]["output"] = doc["company_name"]
    bulkData.append(doc)
    #print "doc: ",doc
    #exit()
    if count < 100:
        count += 1
        print "count: ",count
    else :
        bulkIndex()
        bulkData = []
        count = 1
    
print "ok"
res = client.bulk(bulkData)
print "SUCCESS"